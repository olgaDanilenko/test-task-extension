// get data from local storage
$(function() {
    chrome.storage.local.get(['domainsList'], function(items) {
        if (!chrome.runtime.error) {
            var domainsFromStorage = JSON.parse(items.domainsList);
            for(var i = 0; i < domainsFromStorage.length; i++)
            {
                $('<a/>', {
                    'class':'popup__link',
                    'href': 'http://' + domainsFromStorage[i]['domain'],
                    'target': '_blank',
                    'html': domainsFromStorage[i]['name']
                }).appendTo($('#wrapper'));
            }
        }
    });
});

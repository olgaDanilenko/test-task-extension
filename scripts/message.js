$(function() {
    chrome.storage.local.get(['domainsList', 'alreadyVisitedDomains'], function(items) {
        // pull the list of domains from localstorage
        var domains = JSON.parse(items.domainsList),
            alreadyVisitedDomains = items.alreadyVisitedDomains,
            currentURL = window.location.href;

        for(var i = 0; i < domains.length; i++) {
            if (currentURL.indexOf(domains[i]['domain']) != -1) {
                if(alreadyVisitedDomains[domains[i]['domain']] === undefined){
                    alreadyVisitedDomains[domains[i]['domain']] = 0;
                }

                // array alreadyVisitedDomains recorded the number of pages visited from the list
                alreadyVisitedDomains[domains[i]['domain']]++;

                chrome.storage.local.set({'alreadyVisitedDomains': alreadyVisitedDomains});

                // if the user visited the site from a list of at least three times to show the message
                if(alreadyVisitedDomains[domains[i]['domain']] <= 3){
                    // in HTML loaded is added popup with message
                    $('<div/>', {
                        'id':'alert_wrapper',
                        'html': '<div class="alert-wrapper__box" > ' +
                        '<div class="alert-wrapper__panel"> ' +
                        '<span id="alert_close" class="alert-wrapper__close">x</span> </div>' +
                        '<div class="alert-wrapper__content">' + domains[i]['message'] + '</div> </div>'
                    }).prependTo('body');
                }
            }
        }
    });

    // close popup with message
    $('body').on('click','#alert_close', function() {
        var currentURL = window.location.href;

        $('#alert_wrapper').hide();

        chrome.storage.local.get(['domainsList', 'alreadyVisitedDomains'], function(items) {
            var domains =  JSON.parse(items.domainsList),
                alreadyVisitedDomains = items.alreadyVisitedDomains;

            for (var i = 0; i < domains.length; i++) {
                if (currentURL.indexOf(domains[i]['domain']) != -1) {
                    alreadyVisitedDomains[domains[i]['domain']] = 4;
                }
            }

            chrome.storage.local.set({'alreadyVisitedDomains': alreadyVisitedDomains});
        });
    });
});

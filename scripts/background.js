$(function() {
    // set popup.html in browser
    chrome.browserAction.setPopup({popup: "html/popup.html"});

    // check whether new version is installed
    chrome.runtime.onInstalled.addListener(function(details){
        if(details.reason == "install"){
            var dateTime = new Date();
            chrome.storage.local.set({
                'domainsList': {},
                'lastUpdated': null,
                'alreadyVisitedDomains': {},
                'searchEngines': ['google.ru', 'google.com', 'bing.com']
            });
        }
    });

    chrome.storage.local.set({'alreadyVisitedDomains': {}});

    // loaded list of sites "http://www.softomate.net/ext/employees/list.json" and stored in storage
    var getListDomains = function() {
        xhr = new XMLHttpRequest();
        xhr.open("GET", "http://www.softomate.net/ext/employees/list.json", true);
        xhr.send(null);
        xhr.onreadystatechange = function() {
            if(xhr.readyState == 4){
                if( xhr.responseText ){
                    var data = xhr.responseText;
                    chrome.storage.local.remove(['domainsList']);
                    chrome.storage.local.set({'domainsList': data, 'lastUpdated': new Date().toISOString()}, function() {
                        console.log('Settings saved');
                    });

                }

            }
        }
    };

    chrome.storage.local.get(['lastUpdated'], function(items) {
        var currentDate = new Date();

        if((items.lastUpdated == null) || ((Math.abs(currentDate.getTime() - new Date(items.lastUpdated).getTime())) > 60*60*1000)){
            getListDomains();
        }

        // update the list of sites every hour
        setInterval(getListDomains, 60 * 60 * 1000);
    });

    chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
        if(changeInfo.status == 'complete'){
            chrome.tabs.sendMessage(tabId, {text: 'tab_updated'}, function () {
                console.log('transfer data');
            });
        }
    });

});

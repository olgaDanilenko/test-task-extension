$(function() {

    // xpath selector function
    function _x(STR_XPATH) {
        var xresult = document.evaluate(STR_XPATH, document, null, XPathResult.ANY_TYPE, null),
            xnodes = [],
            xres;

        while (xres = xresult.iterateNext()) {
            xnodes.push(xres);
        }
        return xnodes;
    }

    chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
       if(msg.text === 'tab_updated'){
           chrome.storage.local.get(['domainsList', 'searchEngines'], function(items) {
               // pull the list of domains from localstorage
               var domains = JSON.parse(items.domainsList),
                   searchEngines = items.searchEngines,
                   currentURL = window.location.href,
                   linkText = null,
                   iconLink = chrome.runtime.getURL('/img/icon.png'),
                   elementPath = null;

               for(var i = 0; i < searchEngines.length; i++){
                   if(currentURL.indexOf(searchEngines[i] ) != -1){
                       // specifying the paths to the HTML elements in the search results that contain url
                       if(searchEngines[i] == 'google.ru' || searchEngines[i] == 'google.com'){
                           elementPath = '//cite[contains(@class, "_Rm")]'
                       } else if(searchEngines[i] == 'bing.com'){
                           elementPath = '//div[contains(@class, "b_attribution")]/cite';
                       }
                       if(elementPath){
                           // on sites google.[com|ru], bing.com in the search results is marked with an icon of expansion sites from the downloaded list
                           setTimeout(function () {
                               $(_x(elementPath)).each(function() {
                                   linkText = $(this).text();
                                   for(var i = 0; i < domains.length; i++){
                                       if(linkText.indexOf(domains[i]['domain']) != -1){
                                           $(this).before('<img class="inject-icon" src="' + iconLink + '">');
                                       }
                                   }
                               });
                           }, 2000);
                       }
                       break;
                   }
               }
           });
       }
    });
});
